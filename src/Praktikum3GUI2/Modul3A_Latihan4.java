package Praktikum3GUI2;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Modul3A_Latihan4 extends JFrame {

    public Modul3A_Latihan4() {
        this.setLayout(null);
        this.setSize(300, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JLabel label1 = new JLabel("Keyword");
        label1.setBounds(200, 20, 100, 20);
        this.add(label1);

        JTextField text = new JTextField();
        text.setBounds(150, 50, 150, 20);
        this.add(text);

        JButton tombol = new JButton();
        tombol.setText("Find");
        tombol.setBounds(170, 90, 100, 40);
        this.add(tombol);

    }

    public static void main(String[] args) {
        new Modul3A_Latihan4();
    }
}
