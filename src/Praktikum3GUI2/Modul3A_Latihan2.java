package Praktikum3GUI2;

import javax.swing.JFrame;

public class Modul3A_Latihan2 extends JFrame {

    public Modul3A_Latihan2() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

    }

    public static void main(String[] args) {
        new Modul3A_Latihan2();
    }
}
