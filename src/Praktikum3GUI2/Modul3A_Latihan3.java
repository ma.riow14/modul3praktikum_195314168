package Praktikum3GUI2;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Modul3A_Latihan3 extends JFrame {

    public Modul3A_Latihan3() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label = new JLabel("Ini Label");
        label.setBounds(50, 70, 100, 100);
        this.add(label);

        JTextField field = new JTextField();
        field.setBounds(30, 50, 80, 80);
        this.add(field);

        JCheckBox check1 = new JCheckBox("Ini check/uncheck");
        check1.setBounds(10, 30, 60, 60);
        this.add(check1);

        JRadioButton pilih = new JRadioButton("Ini pilihan");
        pilih.setBounds(70, 90, 120, 120);
        this.add(pilih);

    }

    public static void main(String[] args) {
        new Modul3A_Latihan3();
    }
}
