package Praktikum3GUI2;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Modul3B_Latihan1 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;

    public static void main(String[] args) {
        Modul3B_Latihan1 frame = new Modul3B_Latihan1();
        frame.setVisible(true);
    }

    public Modul3B_Latihan1() {
        Container contentPane = getContentPane();

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Program Modul3B");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);

        JLabel labelNama = new JLabel("Nama ");
        labelNama.setBounds(10, 10, 100, 20);
        JLabel labelKelamin = new JLabel("Jenis Kelamin ");
        labelKelamin.setBounds(10, 40, 100, 20);
        JLabel labelHobby = new JLabel("Hobby ");
        labelHobby.setBounds(10, 70, 100, 20);

        this.add(labelNama);
        this.add(labelKelamin);
        this.add(labelHobby);

        JTextField fieldNama = new JTextField();
        fieldNama.setBounds(120, 10, 190, 20);
        this.add(fieldNama);

        JRadioButton buttonMan = new JRadioButton("Laki-Laki");
        buttonMan.setBounds(120, 40, 100, 20);
        this.add(buttonMan);
        JRadioButton buttonWoman = new JRadioButton("Perempuan");
        buttonWoman.setBounds(220, 40, 100, 20);
        this.add(buttonWoman);

        JCheckBox or1 = new JCheckBox("Olahraga");
        or1.setBounds(120, 70, 125, 20);
        this.add(or1);
        JCheckBox or2 = new JCheckBox("Shopping");
        or2.setBounds(120, 90, 125, 20);
        this.add(or2);
        JCheckBox or3 = new JCheckBox("Computer Games");
        or3.setBounds(120, 110, 125, 20);
        this.add(or3);
        JCheckBox or4 = new JCheckBox("Nonton Bioskop");
        or4.setBounds(120, 113, 125, 50);
        this.add(or4);

        okButton = new JButton("OK");
        okButton.setBounds(100, 170, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);

        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(200, 170, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

}

